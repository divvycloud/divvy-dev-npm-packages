# Building and Publishing Package
- npm config set registry https://nexus.divvycloud.net/repository/npm-internal/
- npm adduser --registry=https://nexus.divvycloud.net/repository/npm-internal/
- yarn install
- npm run build
- cd dist && cp ../package.json .
- npm publish
